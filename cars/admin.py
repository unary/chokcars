from django.contrib import admin
from .models import Vender, Car, Transmition, Carburetor
from django.contrib.flatpages.models import FlatPage
from django.contrib.flatpages.admin import FlatPageAdmin


class VenderAdmin(admin.ModelAdmin):
    exclude = ('slug', )


class CarAdmin(admin.ModelAdmin):
    exclude = ('slug', )


class FlatpageAdmin(FlatPageAdmin):

    class Media:
        js = (
            '/static/js/jquery.js',
            '/static/js/jquery-te-1.4.0.min.js',
            '/static/js/admin.js',
        )
        css = {
            'all': ('/static/css/jquery-te-1.4.0.css', )
        }

admin.site.register(Vender, VenderAdmin)
admin.site.register(Transmition)
admin.site.register(Carburetor)
admin.site.register(Car, CarAdmin)
admin.site.unregister(FlatPage)
admin.site.register(FlatPage, FlatpageAdmin)
