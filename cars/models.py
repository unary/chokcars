from django.db import models
from django.template.defaultfilters import slugify


class Vender(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField()
    created = models.DateField(auto_now_add=True)
    updated = models.DateField(auto_now=True)

    class Meta:
        verbose_name = "Vender"
        verbose_name_plural = "Venders"

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Vender, self).save(*args, **kwargs)

    def __str__(self):
        return self.name


class Carburetor(models.Model):
    name = models.CharField(max_length=255)
    created = models.DateField(auto_now_add=True)
    updated = models.DateField(auto_now=True)

    class Meta:
        verbose_name = "Carburetor"
        verbose_name_plural = "Carburetors"

    def __str__(self):
        return self.name


class Transmition(models.Model):
    name = models.CharField(max_length=255)
    created = models.DateField(auto_now_add=True)
    updated = models.DateField(auto_now=True)

    class Meta:
        verbose_name = "Transmition"
        verbose_name_plural = "Transmitions"

    def __str__(self):
        return self.name


class Car(models.Model):
    vender = models.ForeignKey(Vender)
    model = models.CharField(max_length=255, unique=True)
    slug = models.SlugField()
    image = models.ImageField()
    price = models.DecimalField(
        help_text="Price per day", max_digits=5, decimal_places=2)
    pessengers = models.PositiveIntegerField(help_text="Number of passengers")
    door = models.PositiveIntegerField(help_text="Number of doors")
    transmition = models.ForeignKey(Transmition)
    baggage = models.PositiveIntegerField()
    sterio_cd_mp3 = models.BooleanField(default=False)
    climate_control = models.BooleanField(default=False)
    gps = models.BooleanField(default=False)
    radio = models.BooleanField(default=False)
    carburetor = models.ForeignKey(Carburetor)
    kilometrage = models.PositiveIntegerField()
    maintenance = models.DateField()
    profit = models.DecimalField(max_digits=5, decimal_places=2, null=True, default=0)
    availibility = models.BooleanField(default=True)

    created = models.DateField(auto_now_add=True)
    modified = models.DateField(auto_now=True)

    class Meta:
        verbose_name = "Car"
        verbose_name_plural = "Cars"

    def save(self, *args, **kwargs):
        self.slug = slugify(self.model)
        super(Car, self).save(*args, **kwargs)

    def __str__(self):
        return self.model
