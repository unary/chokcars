from django.views.generic import TemplateView, ListView
from cars.models import Car
#from reservations.models import Reservation
from reservations.forms import ReservationForm
from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from .forms import ContactForm
from django.core.mail import send_mail
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from reservations.models import Reservation


class Home(TemplateView):
    template_name = 'cars/home.html'

    def get_context_data(self, **kwargs):
        context = super(Home, self).get_context_data(**kwargs)
        context['cars'] = Car.objects.all().order_by('?')[:3]
        return context


class Vehicles(ListView):
    model = Car
    template_name = "cars/list.html"
    context_object_name = 'cars'


def vehicle(request, slug):
    if request.method == 'POST':
        form = ReservationForm(request.POST)
        if form.is_valid():
            r = form.save(commit=False)
            car = get_object_or_404(Car, slug=slug)
            r.car = car
            d = r.dropoff_date - r.pickup_date
            r.duration = int(d.days)
            r.amount = car.price * int(d.days)
            r.save()
            return HttpResponseRedirect(reverse('booking', args=[r.code]))
    else:
        form = ReservationForm()

    data = {}
    data['form'] = form
    car = get_object_or_404(Car, slug=slug)
    data['car'] = car
    try:
        reservation = Reservation.objects.filter(
            car=car, status=True).latest('created')
        data['reservation'] = reservation
    except Reservation.DoesNotExist:
        pass
    return render(request, 'cars/detail.html', data)


def notfound(request):
    return render(request, '404.html')


def contact(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            message = form.cleaned_data['message']
            send_mail(
                '[Chokcars] from %s' % (name),
                message,
                email,
                ['info@chokcars.com'],
            )
            messages.success(
                request, 'Your messages has been sent successfully.')
    else:
        form = ContactForm()
    return render(request, 'contact.html', {'form': form})


@csrf_exempt
def success(request):
    return render(request, 'success.html')
