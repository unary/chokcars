# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Car',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('model', models.CharField(max_length=255)),
                ('slug', models.SlugField()),
                ('image', models.ImageField(upload_to=b'')),
                ('price', models.DecimalField(help_text=b'Price per day', max_digits=5, decimal_places=2)),
                ('pessengers', models.PositiveIntegerField(help_text=b'Number of passengers')),
                ('door', models.PositiveIntegerField(help_text=b'Number of doors')),
                ('baggage', models.PositiveIntegerField()),
                ('sterio_cd_mp3', models.BooleanField(default=False)),
                ('climate_control', models.BooleanField(default=False)),
                ('pgs', models.BooleanField(default=False)),
                ('kilometrage', models.PositiveIntegerField()),
                ('maintenance', models.DateField()),
                ('profit', models.DecimalField(default=0, null=True, max_digits=5, decimal_places=2)),
                ('availibility', models.BooleanField(default=True)),
                ('created', models.DateField(auto_now_add=True)),
                ('modified', models.DateField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Car',
                'verbose_name_plural': 'Cars',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Carburetor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('created', models.DateField(auto_now_add=True)),
                ('updated', models.DateField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Carburetor',
                'verbose_name_plural': 'Carburetors',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Transmition',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('created', models.DateField(auto_now_add=True)),
                ('updated', models.DateField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Transmition',
                'verbose_name_plural': 'Transmitions',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Vender',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('slug', models.SlugField()),
                ('created', models.DateField(auto_now_add=True)),
                ('updated', models.DateField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Vender',
                'verbose_name_plural': 'Venders',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='car',
            name='carburetor',
            field=models.ForeignKey(to='cars.Carburetor'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='car',
            name='transmition',
            field=models.ForeignKey(to='cars.Transmition'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='car',
            name='vender',
            field=models.ForeignKey(to='cars.Vender'),
            preserve_default=True,
        ),
    ]
