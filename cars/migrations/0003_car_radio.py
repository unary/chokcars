# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cars', '0002_auto_20141214_0134'),
    ]

    operations = [
        migrations.AddField(
            model_name='car',
            name='radio',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
