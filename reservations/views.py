from django.shortcuts import render, get_object_or_404
from reservations.models import Reservation
from paypal.standard.forms import PayPalPaymentsForm
import random
from django.conf import settings
from django.core.urlresolvers import reverse


def booking(request, code):
    reservation = get_object_or_404(Reservation, code=code)
    paypal_dict = {
        "business": settings.PAYPAL_RECEIVER_EMAIL,
        "amount": reservation.amount,
        "currency_code": "EUR",
        "item_name": reservation.car.model,
        "invoice": random.random(),
        "notify_url": settings.SITE_URL + reverse('paypal-ipn'),
        "return_url": settings.SITE_URL + reverse('success'),
        "cancel_return": settings.SITE_URL + reverse('booking', kwargs={'code': reservation.code}),
        "custom": reservation.code
    }
    form = PayPalPaymentsForm(initial=paypal_dict)
    return render(request, 'reservations/confirmation.html', {'reservation': reservation, 'form': form})
