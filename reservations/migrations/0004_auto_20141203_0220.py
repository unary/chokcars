# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reservations', '0003_reservation_code'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reservation',
            name='dropoff_date',
            field=models.DateTimeField(),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='reservation',
            name='pickup_date',
            field=models.DateTimeField(),
            preserve_default=True,
        ),
    ]
