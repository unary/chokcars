# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('cars', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('created', models.DateField(auto_now_add=True)),
                ('modified', models.DateField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Location',
                'verbose_name_plural': 'Locations',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Reservation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=3)),
                ('first_name', models.CharField(max_length=64)),
                ('last_name', models.CharField(max_length=64)),
                ('email', models.EmailField(max_length=75)),
                ('phone', models.CharField(max_length=15, validators=[django.core.validators.RegexValidator(regex=b'^\\+?1?\\d{9,15}$', message=b'Phone number must be entered in the format: +999999999')])),
                ('address', models.TextField()),
                ('birth_date', models.DateField()),
                ('country', models.CharField(max_length=255)),
                ('flight_number', models.CharField(max_length=255)),
                ('pickup_date', models.DateField()),
                ('dropoff_date', models.DateField()),
                ('message', models.TextField(null=True, blank=True)),
                ('duration', models.PositiveIntegerField()),
                ('amount', models.PositiveIntegerField()),
                ('status', models.BooleanField(default=False)),
                ('car', models.ForeignKey(to='cars.Car')),
                ('dropoff_location', models.ForeignKey(related_name='dropoff_location', to='reservations.Location')),
                ('pickup_location', models.ForeignKey(related_name='pickup_location', to='reservations.Location')),
            ],
            options={
                'verbose_name': 'Reservation',
                'verbose_name_plural': 'Reservations',
            },
            bases=(models.Model,),
        ),
    ]
