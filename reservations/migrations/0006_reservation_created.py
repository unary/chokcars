# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('reservations', '0005_auto_20141213_2225'),
    ]

    operations = [
        migrations.AddField(
            model_name='reservation',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2014, 12, 14, 18, 51, 15, 417487, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
    ]
