# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reservations', '0004_auto_20141203_0220'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reservation',
            name='flight_number',
            field=models.CharField(max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
    ]
