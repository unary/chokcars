# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reservations', '0002_remove_reservation_title'),
    ]

    operations = [
        migrations.AddField(
            model_name='reservation',
            name='code',
            field=models.CharField(default='', max_length=50),
            preserve_default=False,
        ),
    ]
