from django.db import models
from cars.models import Car
from django.core.validators import RegexValidator
from uuid import uuid4
from paypal.standard.ipn.signals import payment_was_successful
from django.template import Context
from django.template.loader import get_template
from django.conf import settings
from django.shortcuts import get_object_or_404
from django.core.mail import EmailMessage


def sending_email(sender, **kwargs):
    ipn_object = sender
    if ipn_object.payment_status == "Completed":
        reservation = get_object_or_404(Reservation, code=ipn_object.custom)
        reservation.status = True
        reservation.save()
        subject = ipn_object.item_name
        to = [ipn_object.payer_email, reservation.email]
        from_email = settings.DEFAULT_FROM_EMAIL
        ctx = {
            'car_model': ipn_object.item_name,
            'amount': ipn_object.mc_gross,
            'first_name': ipn_object.first_name,
            'last_name': ipn_object.last_name,
            'email': ipn_object.payer_email,
            'pickup_location': reservation.pickup_location,
            'pickup_date': reservation.pickup_date,
            'dropoff_location': reservation.dropoff_location,
            'dropoff_date': reservation.dropoff_date,
            'duration': reservation.duration,
            'price': reservation.car.price,
            'invoice': ipn_object.invoice,
            'logo': settings.SITE_URL + '/static/img/logo-invert.png',
            'car_image': settings.SITE_URL + reservation.car.image.url,
        }
        message = get_template('email/template.html').render(Context(ctx))
        msg = EmailMessage(subject, message, to=to, from_email=from_email)
        msg.content_subtype = 'html'
        msg.send()

payment_was_successful.connect(sending_email)


class Location(models.Model):
    name = models.CharField(max_length=50)
    created = models.DateField(auto_now_add=True)
    modified = models.DateField(auto_now=True)

    class Meta:
        verbose_name = "Location"
        verbose_name_plural = "Locations"

    def __str__(self):
        return self.name


class Reservation(models.Model):
    code = models.CharField(max_length=50)
    car = models.ForeignKey(Car)
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64)
    email = models.EmailField()
    phone_regex = RegexValidator(
        regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: +999999999")
    phone = models.CharField(validators=[phone_regex], max_length=15)
    address = models.TextField()
    birth_date = models.DateField()
    country = models.CharField(max_length=255)
    flight_number = models.CharField(max_length=255, blank=True, null=True)
    pickup_location = models.ForeignKey(
        Location, related_name='pickup_location')
    pickup_date = models.DateTimeField()
    dropoff_location = models.ForeignKey(
        Location, related_name='dropoff_location')
    dropoff_date = models.DateTimeField()
    message = models.TextField(blank=True, null=True)
    duration = models.PositiveIntegerField()
    amount = models.PositiveIntegerField()
    status = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = "Reservation"
        verbose_name_plural = "Reservations"

    def save(self, *args, **kwargs):
        if not self.code:
            self.code = uuid4()
        d = self.dropoff_date - self.pickup_date
        self.duration = int(d.days)
        self.amount = self.duration * self.car.price
        super(Reservation, self).save(*args, **kwargs)

    def __str__(self):
        return unicode(self.car)
