# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import Location, Reservation


class ReservationAdmin(admin.ModelAdmin):
    exclude = ('code', )
    list_display = (
        'car', 'code', 'first_name', 'last_name', 'flight_number', 'pickup_location', 'dropoff_location',
        'pickup_date', 'dropoff_date', 'duration_days', 'amount_euro', 'status')

    def amount_euro(self, obj):
        return "%s €" % (obj.amount)
    amount_euro.short_description = 'Amount'

    def duration_days(self, obj):
        return "%s day(s)" % (obj.duration)
    duration_days.short_description = 'Duration'

admin.site.register(Location)
admin.site.register(Reservation, ReservationAdmin)
