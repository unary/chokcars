from django import forms
from .models import Reservation
from django.utils import timezone
import pytz

utc = pytz.UTC


class ReservationForm(forms.ModelForm):
    cg = forms.CharField(widget=forms.CheckboxInput(), required=True)

    class Meta:
        model = Reservation
        exclude = ['code', 'car', 'status', 'amount', 'duration']

    # def clean_pickup_date(self):
    #     pickup_date = self.cleaned_data['pickup_date']
    #     return self.cleaned_data['pickup_date']

    def clean(self):
        dropoff_date = self.cleaned_data.get('dropoff_date')
        pickup_date = self.cleaned_data.get('pickup_date')

        if pickup_date is None or dropoff_date is None:
            raise forms.ValidationError('error dates')
        if pickup_date < timezone.now():
            # raise forms.ValidationError('error date in the past')
            self.add_error('pickup_date', 'Choisissez une date depius le jour courant.')
        if dropoff_date < pickup_date or dropoff_date == pickup_date or dropoff_date < timezone.now():
            # raise forms.ValidationError('error date')
            self.add_error('dropoff_date', 'Choisissez une date ulterieure de la date de livraision.')
        if (dropoff_date - pickup_date).days < 2:
            self.add_error('dropoff_date', 'Minimum est deux jours.')
