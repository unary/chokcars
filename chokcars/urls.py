from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from cars.views import Home, Vehicles

urlpatterns = patterns('',
                       url(r'^$', Home.as_view(), name='home'),
                       url(r'^contact/$', "cars.views.contact",
                           name='contact'),
                       url(r'^chokcars/paypal/',
                           include('paypal.standard.ipn.urls')),
                       url(r'^success/$', "cars.views.success",
                           name='success'),
                       url(r'^vehicles/$', Vehicles.as_view(),
                           name='vehicles'),
                       url(r'^vehicle/(?P<slug>[-_\w]+)/$',
                           'cars.views.vehicle', name="vehicle"),
                       url(r'^booking/(?P<code>[-_\w]+)/$',
                           'reservations.views.booking', name='booking'),
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {
                           'document_root': settings.STATIC_ROOT, 'show_indexes': False}, name='static'),
                       url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
                           'document_root': settings.MEDIA_ROOT, 'show_indexes': False}, name='media'),
                       url(r'^', include('django.contrib.flatpages.urls')),
                       )
